﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1 : MonoBehaviour
{

	public int maxHealth = 100;
	public int currentHealth;

	public HealthBar healthBar;

	// Start is called before the first frame update
	void Start()
	{
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
	}


	void OnCollisionEnter(Collision col)
	{
		if (gameObject.tag.Equals("Bullet"));
		{
			Debug.Log("Collision Detected");
			TakeDamage(20);
		}
	}

	void TakeDamage(int damage)
	{
		currentHealth -= damage;

		healthBar.SetHealth(currentHealth);
	}

	void Update()
	{
		if (currentHealth == 0)
		{
			Destroy(gameObject);
		}
	}
}

